
import { updateAppConfig } from '#app'
import { defuFn } from '/home/siyam/Project/AutoFormation-Nuxt/Exo-Nuxt/Gitlab/formation_nuxtjs/awesome-sidebase-prisma-todolist-project/node_modules/defu/dist/defu.mjs'

const inlineConfig = {}

// Vite - webpack is handled directly in #app/config
if (import.meta.hot) {
  import.meta.hot.accept((newModule) => {
    updateAppConfig(newModule.default)
  })
}



export default /* #__PURE__ */ defuFn(inlineConfig)
